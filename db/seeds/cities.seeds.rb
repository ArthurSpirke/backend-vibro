after :regions do
  start = Time.now
  p "Starting the creation of cities"
  
  country = Country.find_by_name('Ukraine')

  City.create(name: 'Lviv', country: country, region: Region.find(1))

  p "It took #{Time.now - start}s"
end