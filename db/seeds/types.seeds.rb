after :locations do
  start = Time.now
  p "Starting the creation of types"
  
  ['landmark', 'food', 'shopping', 'bus stops', 'pharmacy', 'entertainment', 'green zones'].each do |type|
  	Type.create(name: type)
  end

  p "It took #{Time.now - start}s"
end