after :cities do
  start = Time.now
  p "Starting the creation of districts"
  
  country = Country.find_by_name('Ukraine')

  District.create(name: 'Halytskyi district', country: country, region: Region.find(1), city: City.find(1))

  p "It took #{Time.now - start}s"
end