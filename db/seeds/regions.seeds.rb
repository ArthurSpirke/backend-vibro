after :countries do
  start = Time.now
  p "Starting the creation of regions"
  
  Region.create(name: 'Lvovskaya Oblast', country: Country.find_by_name('Ukraine'))

  p "It took #{Time.now - start}s"
end