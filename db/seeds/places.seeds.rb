after :types do
  start = Time.now
  p "Starting the creation of places"
  
  location =  Location.create(latitude: 49.841892, longitude: 24.031480, district: District.find(1))
  Place.create(city_id: City.find_by_name('Lviv').id, name: 'Rynok square', description: 'This is central place of Lviv', location_id: location.id, type_id: Type.find_by_name('landmark').id)

  location =  Location.create(latitude: 49.848458, longitude: 24.039288, district: District.find(1))
  Place.create(city_id: City.find_by_name('Lviv').id, name: 'High castle', description: 'This is high castle', location_id: location.id, type_id: Type.find_by_name('landmark').id)

  location =  Location.create(latitude: 49.837809, longitude: 24.028376, district: District.find(1))
  Place.create(city_id: City.find_by_name('Lviv').id, name: 'Ratusha Tower', description: 'This is ratusha tower', location_id: location.id, type_id: Type.find_by_name('landmark').id)

  location =  Location.create(latitude: 50.739517, longitude: 25.318047, district: District.find(1))
  Place.create(city_id: City.find_by_name('Lviv').id, name: 'Pharmacy Museum', description: 'This is Pharmacy Museum', location_id: location.id, type_id: Type.find_by_name('pharmacy').id)

  location =  Location.create(latitude: 49.838624, longitude: 24.012836, district: District.find(1))
  Place.create(city_id: City.find_by_name('Lviv').id, name: 'St. George Castle', description: 'This is St. George Castle', location_id: location.id, type_id: Type.find_by_name('landmark').id)

  location =  Location.create(latitude: 49.773749, longitude: 24.011596, district: District.find(1))
  Place.create(city_id: City.find_by_name('Lviv').id, name: 'Ashan', description: 'This is Ashan!!!', location_id: location.id, type_id: Type.find_by_name('shopping').id)

  location =  Location.create(latitude: 49.843942, longitude: 24.026233, district: District.find(1))
  Place.create(city_id: City.find_by_name('Lviv').id, name: 'Theatre', description: 'This is Theatre', location_id: location.id, type_id: Type.find_by_name('entertainment').id)

  location =  Location.create(latitude: 49.841292, longitude: 24.032238, district: District.find(1))
  Place.create(city_id: City.find_by_name('Lviv').id, name: 'Kriivka', description: 'This is Kriivka!', location_id: location.id, type_id: Type.find_by_name('food').id)

  location =  Location.create(latitude: 49.838623, longitude: 24.019809, district: District.find(1))
  Place.create(city_id: City.find_by_name('Lviv').id, name: 'Park of Ivan Franko', description: 'This is Park of Ivan Franko!', location_id: location.id, type_id: Type.find_by_name('green zones').id)

  location =  Location.create(latitude: 49.839786, longitude: 24.022330, district: District.find(1))
  Place.create(city_id: City.find_by_name('Lviv').id, name: 'Bus stop!', description: 'This is Bus STOP!!!', location_id: location.id, type_id: Type.find_by_name('bus stops').id)

  location =  Location.create(latitude: 49.860986, longitude: 24.031149, district: District.find(1))
  Place.create(city_id: City.find_by_name('Lviv').id, name: 'Yard!', description: 'This is Yard', location_id: location.id, type_id: Type.find_by_name('green zones').id)

  p "It took #{Time.now - start}s"
end