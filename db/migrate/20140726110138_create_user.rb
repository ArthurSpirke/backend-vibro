class CreateUser < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :device_uuid
      t.string :first_name
      t.string :last_name
      t.integer :age
    end
  end
end
