class CreateRatingPoint < ActiveRecord::Migration
  def change
    create_table :rating_points do |t|
      t.integer :rating_id
      t.integer :points
      t.string  :type
    end
  end
end
