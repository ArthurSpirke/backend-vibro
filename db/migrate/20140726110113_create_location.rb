class CreateLocation < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.decimal :longitude
      t.decimal :latitude
      t.integer :district_id
    end
  end
end
