collection @places_with_city
attributes :name

child(:places) do
  attributes :name, :description
  child(:location) do
  attributes :longitude, :latitude
  end
  child(:type) do
  attributes :name
end
end

