collection @ratings

node :points do |r|
  r.rating_points.sum(:points)
end

node :user_name do |r|
  r.user.first_name + ' ' + r.user.last_name
end