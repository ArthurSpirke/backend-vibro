# == Schema Information
#
# Table name: cities
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  country_id :integer
#  region_id  :integer
#


class City < ActiveRecord::Base
  belongs_to :country
  belongs_to :region

  has_many :districts
  has_many :places

end
