# == Schema Information
#
# Table name: countries
#
#  id   :integer          not null, primary key
#  name :string(255)
#


class Country < ActiveRecord::Base
	has_many :regions

end
