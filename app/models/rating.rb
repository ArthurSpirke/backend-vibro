# == Schema Information
#
# Table name: ratings
#
#  id      :integer          not null, primary key
#  user_id :integer
#

class Rating < ActiveRecord::Base
  belongs_to :user
  has_many :rating_points
  
end
