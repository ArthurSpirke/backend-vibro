# == Schema Information
#
# Table name: rating_points
#
#  id        :integer          not null, primary key
#  rating_id :integer
#  points    :integer
#  type      :string(255)
#

class RatingPoint < ActiveRecord::Base
  belongs_to :rating
end
