# == Schema Information
#
# Table name: places
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :string(255)
#  location    :string(255)
#  type_id     :integer
#

class Place < ActiveRecord::Base
  belongs_to :type
  belongs_to :location
  
end
