# == Schema Information
#
# Table name: users
#
#  id          :integer          not null, primary key
#  device_uuid :string(255)
#  first_name  :string(255)
#  last_name   :string(255)
#  age         :integer
#


class User < ActiveRecord::Base
  has_one :rating
end
