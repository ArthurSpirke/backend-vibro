# == Schema Information
#
# Table name: districts
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  country_id :integer
#  region_id  :integer
#  city_id    :integer
#

class District < ActiveRecord::Base
	belongs_to :country
	belongs_to :region
    belongs_to :city
    has_one :location

end
