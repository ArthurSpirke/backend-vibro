# == Schema Information
#
# Table name: locations
#
#  id          :integer          not null, primary key
#  longitude   :decimal(, )
#  latitude    :decimal(, )
#  country_id  :integer
#  region_id   :integer
#  city_id     :integer
#  district_id :integer
#

class Location < ActiveRecord::Base
  belongs_to :district

end
