class RatingsController < ApplicationController

  def index
    @ratings = Rating.all
  end

  def by_user
    @rating = User.find(params[:user_id]).rating
  end

end
