class UsersController < ApplicationController

  def create
    device_uuid = params[:device_uuid]

    user = User.create(device_uuid: device_uuid)
    Rating.create(user_id: user.id)
  end

  def update
    user = User.find(params[:id])

    first_name = params[:first_name]
    last_name = params[:last_name]
    age = params[:age].try(:to_i)

    if !age.nil? && age.present?
      user.update(first_name: first_name, last_name: last_name, age: age)
    else
      user.update(first_name: first_name, last_name: last_name)
    end
  end

end
