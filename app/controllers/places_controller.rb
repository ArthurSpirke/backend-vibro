class PlacesController < ApplicationController

  def index
    # longitude = params[:longitude]
    # latitude = params[:latitude]
    # geo_data = Geocoder.search(latitude + ', ' + longitude)
    # city = geo_data[0].address_components[3]['long_name']

    @places_with_city = City.all

  end

  def create
    name = params[:name]
    description = params[:description]
    type = Type.find_by_name(params[:type])
    latitude = params[:latitude]
    longitude = params[:longitude]

    geo_data = Geocoder.search(latitude + ', ' + longitude)
    city = geo_data[0].address_components[3]['long_name']

    location = Location.create(longitude: longitude, latitude: latitude, district: City.find_by_name(city).districts.first)
    Place.create(name: name, description: description, location_id: location.id, type_id: type.id)
  end

end
