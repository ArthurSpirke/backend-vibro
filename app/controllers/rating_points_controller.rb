class RatingPointsController < ApplicationController

  def index
    user = User.find(params[:user_id])

    @rating_points = user.rating.rating_points
  end

  def create
    user = User.find(params[:user_id])
    points = params[:points]
    type = params[:type]

    RatingPoint.create(points: points, type: type, rating: user.rating)
  end

end
