VibroGuide::Application.routes.draw do

 resources :places

 resources :rating_points

 resources :ratings do
  get :by_user, on: :collection 
 end

 resources :users

end
